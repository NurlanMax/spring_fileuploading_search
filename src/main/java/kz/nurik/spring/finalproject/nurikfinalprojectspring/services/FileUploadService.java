package kz.nurik.spring.finalproject.nurikfinalprojectspring.services;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.User;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
    boolean uploadAva(MultipartFile file, User user);
}
