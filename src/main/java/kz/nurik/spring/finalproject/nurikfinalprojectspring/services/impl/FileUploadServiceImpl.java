package kz.nurik.spring.finalproject.nurikfinalprojectspring.services.impl;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.User;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.services.FileUploadService;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.services.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Autowired
    private UserService userService;

    @Value("${targetURL}")
    private String targetURL;
    @Override
    public boolean uploadAva(MultipartFile file, User user) {
        try {

            String fileName=user.getId()+"pic_kartina";
            fileName= DigestUtils.sha1Hex(fileName);

            byte []bytes=file.getBytes();
            Path path= Paths.get(targetURL+fileName+".jpg");
            Files.write(path, bytes);

            user.setAvatarURL(fileName);
            userService.updateAva(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
