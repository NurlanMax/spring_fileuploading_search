package kz.nurik.spring.finalproject.nurikfinalprojectspring.repo;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.City;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.Club;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ClubRepo extends JpaRepository<Club, Long>, JpaSpecificationExecutor<Club> {

    List<Club> findAllByCity_Id(Long cityId);
    List<Club> findAllByCity(City city);

    List<Club> findAllByDeletedAtNullAndCity_Id(Long cityId);
    List<Club> findAllByDeletedAtNullAndCity(City city);
    List<Club> findAllByDeletedAtNull();


    Club findByDeletedAtNullAndId(Long id);
}
