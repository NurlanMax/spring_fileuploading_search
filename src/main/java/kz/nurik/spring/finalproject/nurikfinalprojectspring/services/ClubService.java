package kz.nurik.spring.finalproject.nurikfinalprojectspring.services;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.City;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.Club;

import java.util.List;

public interface ClubService {
    Club addClub(Club club);
    List<Club> getAllClubs();
    Club getClub(Long id);
    Club saveClub(Club club);
    void deleteClub(Club club);

    List<City> getAllCities();
    City getCity(Long id);

    List<Club> searchClubs(String name, Long cityId, Integer uclFrom, Integer uclTo, Integer leagueFrom, Integer leagueTo);
}
