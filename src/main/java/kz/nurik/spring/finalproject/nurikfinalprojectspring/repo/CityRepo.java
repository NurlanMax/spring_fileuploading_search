package kz.nurik.spring.finalproject.nurikfinalprojectspring.repo;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface CityRepo extends JpaRepository<City,Long> {
    List<City> findAllByDeletedAtNull();
    City findByDeletedAtNullAndId(Long id);
}
