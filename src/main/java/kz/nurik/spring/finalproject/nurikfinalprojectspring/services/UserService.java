package kz.nurik.spring.finalproject.nurikfinalprojectspring.services;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User registerUser(User user);
    boolean updatePassword(User user,String oldPass,String newPass);
    User updateAva(User user);
}
