package kz.nurik.spring.finalproject.nurikfinalprojectspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NurikfinalprojectspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(NurikfinalprojectspringApplication.class, args);
    }

}
