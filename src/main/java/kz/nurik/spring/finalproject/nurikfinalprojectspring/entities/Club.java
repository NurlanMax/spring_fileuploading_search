package kz.nurik.spring.finalproject.nurikfinalprojectspring.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "clubs")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Club extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name="ucl_titles")
    private int championsLeagueTitles;
    @Column(name = "league_titles")
    private int leagueTitles;

    @ManyToOne(fetch = FetchType.EAGER)
    private City city;
}
