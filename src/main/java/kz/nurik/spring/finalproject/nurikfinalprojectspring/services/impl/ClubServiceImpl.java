package kz.nurik.spring.finalproject.nurikfinalprojectspring.services.impl;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.City;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.Club;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.repo.CityRepo;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.repo.ClubRepo;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.services.ClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ClubServiceImpl implements ClubService {
    @Autowired
    private CityRepo cityRepo;

    @Autowired
    private ClubRepo clubRepo;

    @Override
    public Club addClub(Club club) {
        if(club.getCity()!=null) {
            City city=cityRepo.findByDeletedAtNullAndId(club.getCity().getId());
            if(city!=null) {
                clubRepo.save(club);
            }
        }
        return null;
    }

    @Override
    public List<Club> getAllClubs() {
        return clubRepo.findAllByDeletedAtNull();
    }

    @Override
    public Club getClub(Long id) {
        return clubRepo.findByDeletedAtNullAndId(id);
    }

    @Override
    public Club saveClub(Club club) {
        return clubRepo.save(club);
    }

    @Override
    public void deleteClub(Club club) {
        club.setDeletedAt(new Date());
        clubRepo.save(club);
    }

    @Override
    public List<Club> searchClubs(String name, Long cityId, Integer uclFrom, Integer uclTo, Integer leagueFrom, Integer leagueTo) {
        Specification specification=(Specification<Club>)(root, criteriaQuery, criteriaBuilder)->criteriaBuilder.like(criteriaBuilder.upper(root.get("name")),"%"+name.toUpperCase()+"%");

        if(cityId!=0) {
            specification=specification.and((Specification<Club>)(root, criteriaQuery, criteriaBuilder)->criteriaBuilder.equal(root.get("city").get("id"), cityId));
        }

        if(uclFrom!=null) {
            specification=specification.and((Specification<Club>)(root, criteriaQuery, criteriaBuilder)->criteriaBuilder.greaterThanOrEqualTo(root.get("championsLeagueTitles"), uclFrom));
        }

        if(uclTo!=null) {
            specification=specification.and((Specification<Club>)(root, criteriaQuery, criteriaBuilder)->criteriaBuilder.lessThanOrEqualTo(root.get("championsLeagueTitles"), uclTo));
        }

        if(leagueFrom!=null){
            specification = specification.and((Specification<Club>)(root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("leagueTitles"), leagueFrom));
        }

        if(leagueTo!=null){
            specification = specification.and((Specification<Club>)(root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("leagueTitles"), leagueTo));
        }


        return clubRepo.findAll(specification);
    }

    @Override
    public List<City> getAllCities() {
        return cityRepo.findAllByDeletedAtNull();
    }

    @Override
    public City getCity(Long id) {
        return cityRepo.findByDeletedAtNullAndId(id);
    }
}
