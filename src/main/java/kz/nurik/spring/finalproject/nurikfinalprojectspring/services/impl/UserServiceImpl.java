package kz.nurik.spring.finalproject.nurikfinalprojectspring.services.impl;

import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.Role;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.entities.User;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.repo.RoleRepo;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.repo.UserRepo;
import kz.nurik.spring.finalproject.nurikfinalprojectspring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RoleRepo roleRepo;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = userRepo.findByEmail(s);

        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("User not found");
        }

    }

    @Override
    public User registerUser(User user) {
        User checkUser = userRepo.findByEmail(user.getEmail());
        if (checkUser == null) {
            List<Role> roles = new ArrayList<>();
            Role role = roleRepo.findByRole("ROLE_USER");
            roles.add(role);
            user.setRoles(roles);
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            User result = userRepo.save(user);
            return result;
        }
        return null;
    }

    @Override
    public boolean updatePassword(User user, String oldPass, String newPass) {

        if(bCryptPasswordEncoder.matches(oldPass, user.getPassword())){

            user.setPassword(bCryptPasswordEncoder.encode(newPass));
            userRepo.save(user);
            return true;

        }

        return false;

    }

    @Override
    public User updateAva(User user) {
        User checkUser = userRepo.findByEmail(user.getEmail());
        if (checkUser != null) {
            checkUser.setAvatarURL(user.getAvatarURL());
           return userRepo.save(checkUser);
        }
        return null;
    }
}
